//
//  NetworkingTests.swift
//  NetworkingTests
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import XCTest
import Networking

class NetworkingTests: XCTestCase {
    
    private let sessionFake = URLSessionFake()
    private lazy var networking = Networking(session: sessionFake)
    private let urlRequest = URLRequest(url: URL(string: "https://vladislavivanov.networking.com")!)
    
    func test_result_shouldshouldExecuteCompletion_withData_onCorrectStatusCode() {
        let expected = Data(count: 1)
        let statusCode = 200
        
        sessionFake.data = expected
        sessionFake.response = HTTPURLResponseFake(urlRequest.url!, statusCode: statusCode)
        
        networking
            .request(urlRequest)
            .validate(statusCode: statusCode)
            .result { result in
                switch result {
                case .success(let sut):
                    XCTAssertEqual(expected, sut)
                    
                case .failure:
                    XCTFail("Status code validation should finish with success")
                }
        }
    }
    
    func test_result_shouldshouldExecute_completion_withStatusCodeInvalidError_onIncorrectStatusCode() {
        sessionFake.data = Data(count: 1)
        sessionFake.response = HTTPURLResponseFake(urlRequest.url!, statusCode: 500)
        
        let expected = NetworkingError.statusCodeInvalid
        
        networking
            .request(urlRequest)
            .validate(statusCode: 200)
            .result { result in
                switch result {
                case .success:
                    XCTFail("Status code validation should finish with failure")
                    
                case .failure(let sut):
                    XCTAssertEqual(expected, sut as! NetworkingError)
                }
        }
    }
    
    func test_result_shouldExecute_completion_withEmptyDataError_onEmptyData() {
        sessionFake.data = Data(count: 0)
        sessionFake.response = HTTPURLResponseFake(urlRequest.url!, statusCode: 200)
        
        let expected = NetworkingError.emptyData
        
        networking
            .request(urlRequest)
            .result { result in
                switch result {
                case .success:
                    XCTFail("Status code validation should finish with failure")
                    
                case .failure(let sut):
                    XCTAssertEqual(expected, sut as! NetworkingError)
                }
        }
    }
    
    func test_result_shouldExecute_completion_withCorruptedDataError_onNilData() {
        sessionFake.response = HTTPURLResponseFake(urlRequest.url!, statusCode: 200)
        
        let expected = NetworkingError.corruptedData
        
        networking
            .request(urlRequest)
            .result { result in
                switch result {
                case .success:
                    XCTFail("Status code validation should finish with failure")
                    
                case .failure(let sut):
                    XCTAssertEqual(expected, sut as! NetworkingError)
                }
        }
    }
    
    func test_result_shouldExecute_completion_withError_onError() {
        let expected = NSError(domain: "", code: -1, userInfo: nil)
        sessionFake.error = expected as Error
                
        networking.request(urlRequest)
            .result { result in
                switch result {
                case .success:
                    XCTFail("Status code validation should finish with failure")
                    
                case .failure(let sut):
                    XCTAssertEqual(expected, sut as NSError)
                }
        }
    }
    
    func test_result_shouldReturn_dataTask_onCorrectRequest() {
        let expected = URLSessionDataTaskFake()
        sessionFake.dataTask = expected
                
        let sut = networking.request(urlRequest).result { _ in }
        
        XCTAssertEqual(expected, sut)
    }
    
    func test_result_shouldReturn_nil_onIncorrectRequest() {
        let sut = networking.result { _ in }
        
        XCTAssertNil(sut)
    }
    
}
