//
//  RequestBuilderTests.swift
//  NetworkingTests
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import XCTest
import Networking
import NetworkingTest

class RequestBuilderTests: XCTestCase {
    
    private let apiURL = URL(string: "https://vladislavivanov.networking.com")!
    private let apiVersion = "v1"
    private lazy var urlString = apiURL.appendingPathComponent(apiVersion).absoluteString
    private let apiEnviromentMock = APIEnviromentTypeMock()
    private lazy var builder = RequestBuilder(apiEnviromentMock)
    
    override func setUp() {
        super.setUp()
        
        apiEnviromentMock.underlyingApiURL = apiURL
        apiEnviromentMock.apiVersion = apiVersion
    }
    
    func test_build_shouldCreate_correctURLRequest_onExistedAPIVersion() {
        let expected = urlString
        let sut = builder.build()
        
        XCTAssertEqual(expected, sut.url?.absoluteString)
    }
    
    func test_build_shouldCreate_correctURLRequest_onNilAPIVersion() {
        let expected = apiURL.absoluteString
        apiEnviromentMock.apiVersion = nil
        
        let sut = builder.build()
        
        XCTAssertEqual(expected, sut.url?.absoluteString)
    }
    
    func test_addPathComponent_shouldAdd_correctComponent_onOneComponent() {
        let pathComponent = "test"
        let expected = urlString.appending("/\(pathComponent)")
        
        let sut = builder.add(pathComponent: pathComponent).build()
        
        XCTAssertEqual(expected, sut.url?.absoluteString)
    }
    
    func test_addPathComponent_shouldAdd_correctComponents_onTwoComponents() {
        let first = "test"
        let second = "user"
        let expected = urlString.appending("/\(first)/\(second)")
        
        let sut = builder.add(pathComponent: first, second).build()
        
        XCTAssertEqual(expected, sut.url?.absoluteString)
    }
    
    func test_addURLQuery_shouldAdd_correctParameter_onOneParameter() {
        let queryParameter = URLQueryTypeStub1()
        let expected = urlString.appending("?centerPoint=52.520008%2C13.404954")
        
        let sut = builder.add(urlQuery: queryParameter).build()
        
        XCTAssertEqual(expected, sut.url?.absoluteString)
    }
    
    func test_addURLQuery_shouldAdd_correctParameters_onTwpParameters() {
        let queryParameter = URLQueryTypeStub2()
        let expectedFirstParam = "radius=50"
        let expectedSecondParam = "centerPoint=52.520008%2C13.404954"
        let expectedConnector = "&"
        
        let sut = builder.add(urlQuery: queryParameter).build()
        
        XCTAssert(sut.url!.query!.contains(expectedFirstParam), "URL should containt expected query parameter")
        XCTAssert(sut.url!.query!.contains(expectedSecondParam), "URL should containt expected query parameter")
        XCTAssert(sut.url!.query!.contains(expectedConnector), "URL should containt expected query parameter")
    }
    
    func test_build_shouldAdd_JSONResponseTypeHeader_onDefault() {
        let expectedKey = "Accept"
        let expectedValue = "application/json"
        let sut = builder.build()

        XCTAssertEqual(sut.allHTTPHeaderFields?[expectedKey], expectedValue)
    }
    
    func test_build_shouldNotAdd_ResponseTypeHeader_onNoneResponseType() {
        let sut = builder.build(responseType: .none)

        XCTAssertNil(sut.allHTTPHeaderFields?["Accept"])
    }
    
    func test_addHeader_shouldAdd_correctHeader_onOneHeader() {
        let expected = URLRequestHeaderTypeStub1()
        let sut = builder.add(header: expected).build()
        
        XCTAssertEqual(expected.header.values.first!, sut.allHTTPHeaderFields?[expected.header.keys.first!])
    }
    
    func test_addHeader_shouldAdd_correctHeaders_onTwoHeaders() {
        let expected = URLRequestHeaderTypeStub2()
        let sut = builder.add(header: expected).build()
        let keys = expected.header.keys.compactMap { $0 }
        
        XCTAssertEqual(expected.header[keys[0]], sut.allHTTPHeaderFields?[keys[0]])
        XCTAssertEqual(expected.header[keys[1]], sut.allHTTPHeaderFields?[keys[1]])
    }
    
}
