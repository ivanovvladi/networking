//
//  DataManagerTests.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import XCTest
import Networking
import NetworkingTest

class DataMangerTests: XCTestCase {

    private let urlRequest = URLRequest(url: URL(string: "https://vladislavivanov.networking.com")!)
    private let networkingMock = NetworkingTypeMock()
    private let dataStorageMock = DataStorageTypeMock()
    private let requestBuilderMock = RequestBuilderTypeMock()
    private lazy var dataManager = DataManager(networking: networkingMock,
                                               dataStorage: dataStorageMock,
                                               requestBuilder: requestBuilderMock)
    
    override func setUp() {
        super.setUp()
        
        dataManager.fetch(urlRequest)
        dataStorageMock.fetchDataForReturnValue = .success(Data())
    }

    func test_fetchCache_shouldCall_dataStorageFetchData_withCorrectParameters() {
        let expectedCalled = true
        let expectedRequest = urlRequest
                
        let sut = dataStorageMock
        dataManager.fetchCache(for: expectedRequest)
        
        XCTAssertEqual(expectedCalled, sut.fetchDataForCalled)
        XCTAssertEqual(expectedRequest, sut.fetchDataForReceivedUrlRequest)
    }
    
    func test_updateCache_shouldCall_dataStorageUpdateData_withCorrectParameters() {
        let expectedCalled = true
        let expectedData = Data(count: 1)
        let expectedRequest = urlRequest
        
        dataManager.updateCache(for: expectedRequest, data: expectedData)
        
        let sut = dataStorageMock
        
        XCTAssertEqual(expectedCalled, sut.updateDataForDataCalled)
        XCTAssertEqual(expectedData, sut.updateDataForDataReceivedArguments?.data)
        XCTAssertEqual(expectedRequest, sut.updateDataForDataReceivedArguments?.urlRequest)
    }
    
    func test_cleanCache_shouldCall_dataStorageCleanData_withCorrectParameters() {
        let expectedCalled = true
        let expectedRequest = urlRequest
        
        dataManager.cleanCache(for: expectedRequest)
        
        let sut = dataStorageMock
        
        XCTAssertEqual(expectedCalled, sut.cleanDataForCalled)
        XCTAssertEqual(expectedRequest, sut.cleanDataForReceivedUrlRequest)
    }
    
    func test_cleanAllCache_shouldCall_dataStoragecleanAll() {
        let expectedCalled = true
        
        dataManager.cleanAllCache()
        
        let sut = dataStorageMock
        
        XCTAssertEqual(expectedCalled, sut.cleanAllCalled)
    }

    func test_fetch_shouldAdd_urlRequest() {
        let expected = urlRequest
        
        dataManager.result(completionHandler: { _ in })
        
        let sut = networkingMock
        
        XCTAssertEqual(expected, sut.requestReceivedUrlRequest)
    }
    
    func test_validate_shouldAdd_statusCode() {
        let expected = 200
        
        dataManager.validate(statusCode: expected).result(completionHandler: { _ in })
        
        let sut = networkingMock
        
        XCTAssertEqual(expected, sut.validateStatusCodeReceivedStatusCode)
    }
    
    func test_validate_shouldAdd_statusCodes() {
        let expected = 200..<300
        
        dataManager.validate(statusCodes: expected).result(completionHandler: { _ in })
        
        let sut = networkingMock
        
        XCTAssertEqual(expected, sut.validateStatusCodesReceivedStatusCodes)
    }
    
    func test_result_shouldUpdate_cache_withCorrectParameters_onRequired() {
        let expectedCalled = true
        let expectedData = Data(count: 1)
        let expectedRequest = urlRequest
        
        dataManager.updateCache().result(completionHandler: { _ in })
        networkingMock.resultCompletionHandlerReceivedCompletionHandler?(.success(expectedData))
        
        let sut = dataStorageMock
        
        XCTAssertEqual(expectedCalled, sut.updateDataForDataCalled)
        XCTAssertEqual(expectedData, sut.updateDataForDataReceivedArguments?.data)
        XCTAssertEqual(expectedRequest, sut.updateDataForDataReceivedArguments?.urlRequest)
    }
    
    func test_result_shouldNotUpdate_cache_onNotRequired() {
        let expectedCalled = false
        
        dataManager.result(completionHandler: { _ in })
        
        let sut = dataStorageMock
        
        XCTAssertEqual(expectedCalled, sut.updateDataForDataCalled)
    }
    
    func test_result_shouldUse_cacheOnly_withCorrectParameters_onRequired() {
        let expectedNetworkingCalled = false
        let expectedDataStorageCalled = true
        let expectedRequest = urlRequest
        
        let networkingSut = networkingMock
        let dataStorageSut = dataStorageMock
        
        dataManager.useCacheOnly().result(completionHandler: { _ in })
        
        XCTAssertEqual(expectedNetworkingCalled, networkingSut.resultCompletionHandlerCalled)
        XCTAssertEqual(expectedDataStorageCalled, dataStorageSut.fetchDataForCalled)
        XCTAssertEqual(expectedRequest, dataStorageSut.fetchDataForReceivedUrlRequest)
    }
    
    func test_result_shouldNotUse_cacheOnly_onNotRequired() {
        let expectedNetworkingCalled = true
        let expectedDataStorageCalled = false
                
        let networkingSut = networkingMock
        let dataStorageSut = dataStorageMock
        
        dataManager.result(completionHandler: { _ in })
                
        XCTAssertEqual(expectedNetworkingCalled, networkingSut.resultCompletionHandlerCalled)
        XCTAssertEqual(expectedDataStorageCalled, dataStorageSut.fetchDataForCalled)
    }
    
    func test_result_shouldUse_cacheOnError_withCorrectParameters_onRequired() {
        let expectedNetworkingCalled = true
        let expectedDataStorageCalled = true
        let expectedRequest = urlRequest
        var expectedCompletionCalled = false
        let error = NetworkingError.emptyData
        let completionHandler: (Result<Data, Error>) -> Void = { _ in
             expectedCompletionCalled = true
        }
        
        let networkingSut = networkingMock
        let dataStorageSut = dataStorageMock
        
        dataStorageSut.fetchDataForReturnValue = .success(Data())
        dataManager.useCacheOnError().result(completionHandler: completionHandler)
        networkingSut.resultCompletionHandlerReceivedCompletionHandler?(.failure(error))
                
        XCTAssertEqual(expectedNetworkingCalled, networkingSut.resultCompletionHandlerCalled)
        XCTAssertEqual(expectedDataStorageCalled, dataStorageSut.fetchDataForCalled)
        XCTAssertEqual(expectedRequest, dataStorageSut.fetchDataForReceivedUrlRequest)
        XCTAssert(expectedCompletionCalled, "Data manager should call completion handler")
    }
    
    func test_result_shouldNotUse_cacheOnError_onNotRequired() {
        let expectedNetworkingCalled = true
        let expectedDataStorageCalled = false
        let error = NetworkingError.emptyData
        
        let networkingSut = networkingMock
        let dataStorageSut = dataStorageMock
        
        networkingSut.resultCompletionHandlerReceivedCompletionHandler?(.failure(error))
        dataManager.result(completionHandler: { _ in })
        
        XCTAssertEqual(expectedNetworkingCalled, networkingSut.resultCompletionHandlerCalled)
        XCTAssertEqual(expectedDataStorageCalled, dataStorageSut.fetchDataForCalled)
    }
    
    func test_result_shouldExecute_completion_withCorruptedURLRequestError_onNilRequest() {
        let expected = NetworkingError.corruptedURLRequest
        var result: Result<Data, Error>!
        
        // This is needed to reset request properties because URL is set up before each test
        dataManager.result(completionHandler: { _ in })
        dataManager.result(completionHandler: { result = $0 })
        
        XCTAssertEqual(expected, result.error as! NetworkingError)
    }
        
    func test_result_shouldExecute_completion_withDataManagerUnavailableError_onNilDataManager() {
        let expected = NetworkingError.dataManagerUnavailable
        var result: Result<Data, Error>!
        
        var dataManager: DataManager? = .init(networking: networkingMock,
                                              dataStorage: dataStorageMock,
                                              requestBuilder: requestBuilderMock)
        
        dataManager?.fetch(urlRequest).result(completionHandler: { result = $0 })
        dataManager = nil
        networkingMock.resultCompletionHandlerReceivedCompletionHandler?(.success(Data()))
        
        XCTAssertEqual(expected, result.error as! NetworkingError)
    }
    
    func test_result_shouldExecute_completion_withError_onError() {
        let expected = NetworkingError.statusCodeInvalid
        var result: Result<Data, Error>!
        
        dataManager.result(completionHandler: { result = $0 })
        networkingMock.resultCompletionHandlerReceivedCompletionHandler?(.failure(expected))
        
        XCTAssertEqual(expected, result.error as! NetworkingError)
    }
    
    func test_result_shouldExecute_completion_withData_onSuccess() {
        let expected = Data(count: 1)
        var result: Result<Data, Error>!
        
        dataManager.result(completionHandler: { result = $0 })
        networkingMock.resultCompletionHandlerReceivedCompletionHandler?(.success(expected))
        
        XCTAssertEqual(expected, result.value)
    }
    
}
