//
//  RequestBuilder+Stub.swift
//  NetworkingTests
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import Networking

struct URLQueryTypeStub1: URLQueryType {
    
    var query: [String: String] {
        return ["centerPoint": "52.520008,13.404954"]
    }

}

struct URLQueryTypeStub2: URLQueryType {
    
    var query: [String: String] {
        return ["centerPoint": "52.520008,13.404954", "radius": "50"]
    }

}

struct URLRequestHeaderTypeStub1: URLRequestHeaderType {
    
    var header: [String: String] {
        return ["Authorization": "Bearer 0b79bab50daca910b000d4f1a2b675d604257e42"]
    }
    
}

struct URLRequestHeaderTypeStub2: URLRequestHeaderType {
    
    var header: [String: String] {
        return ["Authorization": "Bearer 0b79bab50daca910b000d4f1a2b675d604257e42", "from": "test@user.com"]
    }
    
}
