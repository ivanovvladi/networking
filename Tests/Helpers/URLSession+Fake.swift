//
//  URLSession+Fake.swift
//  NetworkingTests
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import Foundation

class URLSessionFake: URLSession {
    
    typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void
    var data: Data?
    var response: URLResponse?
    var error: Error?
    var dataTask = URLSessionDataTaskFake()
    
    override func dataTask(with request: URLRequest,
                           completionHandler: @escaping (CompletionHandler)) -> URLSessionDataTask {
        
        dataTask.completionHandler = { [weak self] in
            completionHandler(self?.data, self?.response, self?.error)
        }
        
        return dataTask
    }
    
}

class HTTPURLResponseFake: HTTPURLResponse {
    
    convenience init?(_ url: URL,
                      statusCode: Int,
                      httpVersion: String? = nil,
                      headerFields: [String: String]? = nil) {
        
        self.init(url: url,
                  statusCode: statusCode,
                  httpVersion: httpVersion,
                  headerFields: headerFields)
    }
    
}

class URLSessionDataTaskFake: URLSessionDataTask {
    
    var completionHandler: (() -> Void)?
    
    override func resume() {
        completionHandler?()
    }
    
}
