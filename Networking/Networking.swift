//
//  Networking.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import Foundation

/** Allows communication with server
 # Example of usage: #
 
 ```
 Networking(URLSession)
 .request(URLRequest)
 .validate(200..<300)
 .result { result in ... }
 ```
 */
public protocol NetworkingType {
    
    /// Returns singleton instance of Self
    static var shared: NetworkingType { get }
    
    /**
     Returns instance of Self and adds URL request to execute
     
     - Parameters:
     - urlRequest: URLRequest
     
     - Returns: instance of Self
     */
    @discardableResult
    func request(_ urlRequest: URLRequest) -> NetworkingType
    
    /**
     Returns instance of Self and adds status codes to validate
     
     - Parameters:
     - statusCode: Range of status codes to validate (eg.: "200..<300")
     
     - Returns: instance of Self
     */
    @discardableResult
    func validate(statusCodes: Range<Int>?) -> NetworkingType
    
    /**
     Returns instance of Self and adds status code to validate
     
     - Parameters:
     - statusCode: Single status code to validate (eg.: "200")
     
     - Returns: instance of Self
     */
    @discardableResult
    func validate(statusCode: Int?) -> NetworkingType
    
    /**
     Returns URLSessionDataTask if avaliable and executes URL request which has been added by calling fetch
     
     - Parameters:
     - completionHandler: Closure to be executed once the request has finished or failed
     
     - Returns: optional URLSessionDataTask
     */
    @discardableResult
    func result(completionHandler: @escaping (Result<Data, Error>) -> Void) -> URLSessionDataTask?
    
}

// MARK: - Networking

/** Allows communication with server
 # Example of usage: #
 
 ```
 Networking(URLSession)
 .request(URLRequest)
 .validate(200..<300)
 .result { result in ... }
 ```
 */
open class Networking: NetworkingType {
    
    // MARK: - Properties
    
    /// URLSession which provides API for downloading content
    private let session: URLSession
    
    /// URL request for which the data will be downloaded
    private var request: URLRequest?
    
    /// Status code to validate (eg.: "200")
    private var acceptableStatusCode: Int?
    
    /// Status codes to validate (eg.: "200..<300")
    private var acceptableStatusCodes: Range<Int>?
    
    // MARK: - Public properties
    public static let shared: NetworkingType = Networking()
    
    // MARK: - Init
    
    /**
     Create instance of Self
     
     - Parameters:
     - session: URLSession, default is `URLSession.shared`
     */
    public init(session: URLSession = .shared) {
        self.session = session
    }
    
    // MARK: - Interface
    
    @discardableResult
    open func request(_ urlRequest: URLRequest) -> NetworkingType {
        request = urlRequest
        
        return self
    }
    
    @discardableResult
    open func validate(statusCodes: Range<Int>?) -> NetworkingType {
        acceptableStatusCodes = statusCodes
        
        return self
    }
    
    @discardableResult
    open func validate(statusCode: Int?) -> NetworkingType {
        acceptableStatusCode = statusCode
        
        return self
    }
    
    @discardableResult
    open func result(completionHandler: @escaping (Result<Data, Error>) -> Void) -> URLSessionDataTask? {
        
        // Copy properties
        
        let copiedRequest = request
        let copiedAcceptableStatusCode = acceptableStatusCode
        let copiedAcceptableStatusCodes = acceptableStatusCodes
        
        reset()
        
        // Check if URLRequest is available
        
        guard let request = copiedRequest else {
            completionHandler(.failure(NetworkingError.corruptedURLRequest))
            
            return nil
        }
        
        // Create URLSessionDataTask
        
        let dataTask = session.dataTask(with: request) { data, response, error in
            
            // Check if an error was returned during execution
            
            if let error = error {
                completionHandler(.failure(error))
                
                return
            }
            
            // Validate status code of response if avaliable
            
            if let validated = response?.validate(statusCode: copiedAcceptableStatusCode,
                                                  statusCodeRange: copiedAcceptableStatusCodes) {
                
                switch validated {
                case .success:
                    break
                    
                case .failure(let error):
                    completionHandler(.failure(error))
                    
                    return
                }
            }
            
            // Check returned data
            
            guard let data = data else {
                completionHandler(.failure(NetworkingError.corruptedData))
                
                return
            }
            
            guard !data.isEmpty else {
                completionHandler(.failure(NetworkingError.emptyData))
                
                return
            }
            
            completionHandler(.success(data))
        }
        
        // Start or resume URLSessionDataTask
        
        dataTask.resume()
        
        return dataTask
    }
    
}

// MARK: - Helpers

extension Networking {
    
    /// Resets temporary parameters for current request
    fileprivate func reset() {
        request = nil
        acceptableStatusCode = nil
        acceptableStatusCodes = nil
    }
    
}

extension URLResponse {
    
    /**
     Validates status code of response if avaliable
     
     - Parameters:
     - statusCode: Single status code to validate (eg.: "200")
     - statusCodeRange: Range of status codes to validate (eg.: "200..<300")
     
     - Returns:
     `case success(Void)` if validation finished with success
     
     or
     
     `case failure(Error)` if validation finished with error
     */
    fileprivate func validate(statusCode: Int?,
                              statusCodeRange: Range<Int>?) -> Result<Void, Error> {
        
        guard let response = self as? HTTPURLResponse else {
            return .failure(NetworkingError.urlResponseCorrupted)
        }
        
        var result: Result<Void, Error>
        
        if let statusCode = statusCode, statusCode ~= response.statusCode {
            result = .success(Void())
        } else if let statusCode = statusCodeRange, statusCode ~= response.statusCode {
            result = .success(Void())
        } else if statusCode == nil, statusCodeRange == nil {
            result = .success(Void())
        } else {
            result = .failure(NetworkingError.statusCodeInvalid)
        }
        
        return result
    }
    
}
