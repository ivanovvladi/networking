//
//  DataManager.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import Foundation

/**
 Allows fetching data from online or offline
 
 # Example: #
 
 ```
 dataManager
 .fetch(request)
 .validate(200..<300)
 .useCacheOnError()
 .result { result in
 // ...
 }
 ```
 */
public protocol DataManagerType: class {
    
    /// DataCoder which provides API for decoding/encoding
    var dataCoder: DataCoderType { get }
    
    /// RequestBuilder which provides API for building URL request
    var requestBuilder: RequestBuilderType { get }
    
    /**
     Returns result of fetching offline data for URL request with data or error
     - Parameters:
     - urlRequest: URLRequest to execute
     
     - Returns: Result<Data, Error>
     */
    @discardableResult
    func fetchCache(for urlRequest: URLRequest) -> Result<Data, Error>
    
    /**
     Returns instance of Self and updates cache for URL request with data
     
     - Parameters:
     - urlRequest: URL request for which cache should be updated
     - data: Data which should be saved
     
     - Returns: instance of Self
     */
    @discardableResult
    func updateCache(for urlRequest: URLRequest, data: Data) -> DataManagerType
    
    /**
     Returns instance of Self and cleans cache for URL request
     
     - Parameters:
     - urlRequest: URL request for which cache should be cleaned
     
     - Returns: instance of Self
     */
    @discardableResult
    func cleanCache(for urlRequest: URLRequest) -> DataManagerType
    
    /**
     Returns instance of Self and cleans whole stored cache
     
     - Returns: instance of Self
     */
    @discardableResult
    func cleanAllCache() -> DataManagerType
    
    /**
     Returns instance of Self and adds URL request to execute
     
     - Parameters:
     - urlRequest: URLRequest
     
     - Returns: instance of Self
     */
    @discardableResult
    func fetch(_ urlRequest: URLRequest) -> DataManagerType
    
    /**
     Returns instance of Self and adds status codes to validate
     
     - Parameters:
     - statusCode: Range of status codes to validate (eg.: "200..<300")
     
     - Returns: instance of Self
     */
    @discardableResult
    func validate(statusCode: Int) -> DataManagerType
    
    /**
     Returns instance of Self and adds status code to validate
     
     - Parameters:
     - statusCode: Single status code to validate (eg.: "200")
     
     - Returns: instance of Self
     */
    @discardableResult
    func validate(statusCodes: Range<Int>) -> DataManagerType
    
    /**
     Returns instance of Self and adds flag to update cache on receiving URL response
     
     - Returns: instance of Self
     */
    @discardableResult
    func updateCache() -> DataManagerType
    
    /**
     Returns instance of Self and adds flag to use cache on receiving URL response error
     
     - Returns: instance of Self
     */
    @discardableResult
    func useCacheOnError() -> DataManagerType
    
    /**
     Returns instance of Self and adds  flag to use cache even if API is available
     
     - Returns: instance of Self
     */
    @discardableResult
    func useCacheOnly() -> DataManagerType
    
    /**
     Returns instance of Self and executes URL request which has been added by calling fetch
     
     - Parameters:
     - completionHandler: Closure to be executed once the request has finished or failed
     
     - Returns: instance of Self
     */
    @discardableResult
    func result(completionHandler: @escaping (Result<Data, Error>) -> Void) -> DataManagerType
    
}

/**
 Allows fetching data from online or offline
 
 # Example: #
 
 ```
 dataManager
 .fetch(request)
 .validate(200..<300)
 .useCacheOnError()
 .result { result in
 // ...
 }
 ```
 */
open class DataManager: DataManagerType {
    
    // MARK: - Types
    
    /// Defines Request properties
    private struct RequestProperties {
        
        /// URL request for which the data will be downloaded
        var request: URLRequest?
        
        /// Status code to validate (eg.: "200")
        var acceptableStatusCode: Int?
        
        /// Status codes to validate (eg.: "200")
        var acceptableStatusCodes: Range<Int>?
        
        /// Indicates binding of updating cache
        var shouldUpdateCache: Bool = false
        
        /// Indicates binding of using cache on URL response
        var shouldUseCacheOnError: Bool = false
        
        /// Indicates binding of using cache only
        var shouldUseCacheOnly: Bool = false
    }
    
    // MARK: Properties
    
    /// Provides API for downloading online content
    private let networking: NetworkingType
    
    /// Provides API for downloading offline content
    private let dataStorage: DataStorageType
    
    /// Request properties
    private var requestProperties = RequestProperties()
    
    // MARK: - Public properties
    
    public let dataCoder: DataCoderType
    public let requestBuilder: RequestBuilderType
    
    // MARK: - Life cycle
    
    /**
     Create instance of Self
     
     - Parameters:
     - networking: NetworkingType, default is default `Networking.shared`
     - dataStorage: DataStorageType, default is default `DataStorage.shared`
     - dataCoder: DataCoderType, default is `DataCoder.shared`
     - requestBuilder: RequestBuilderType
     */
    public init(networking: NetworkingType = Networking.shared,
                dataStorage: DataStorageType = DataStorage.shared,
                dataCoder: DataCoderType = DataCoder.shared,
                requestBuilder: RequestBuilderType) {
        
        self.networking = networking
        self.dataStorage = dataStorage
        self.dataCoder = dataCoder
        self.requestBuilder = requestBuilder
    }
    
    // MARK: - Interface
    // MARK: - Offline
    
    @discardableResult
    open func fetchCache(for urlRequest: URLRequest) -> Result<Data, Error> {
        dataStorage.fetchData(for: urlRequest)
    }
    
    @discardableResult
    open func updateCache(for urlRequest: URLRequest, data: Data) -> DataManagerType {
        dataStorage.updateData(for: urlRequest, data: data)
        return self
    }
    
    @discardableResult
    open func cleanCache(for urlRequest: URLRequest) -> DataManagerType {
        dataStorage.cleanData(for: urlRequest)
        return self
    }
    
    @discardableResult
    open func cleanAllCache() -> DataManagerType {
        dataStorage.cleanAll()
        return self
    }
    
    // MARK: - Online
    
    @discardableResult
    open func fetch(_ urlRequest: URLRequest) -> DataManagerType {
        requestProperties.request = urlRequest
        return self
    }
    
    @discardableResult
    open func validate(statusCode: Int) -> DataManagerType {
        requestProperties.acceptableStatusCode = statusCode
        return self
    }
    
    @discardableResult
    open func validate(statusCodes statusCode: Range<Int>) -> DataManagerType {
        requestProperties.acceptableStatusCodes = statusCode
        return self
    }
    
    @discardableResult
    open func updateCache() -> DataManagerType {
        requestProperties.shouldUpdateCache = true
        return self
    }
    
    @discardableResult
    open func useCacheOnError() -> DataManagerType {
        requestProperties.shouldUseCacheOnError = true
        return self
    }
    
    @discardableResult
    open func useCacheOnly() -> DataManagerType {
        requestProperties.shouldUseCacheOnly = true
        return self
    }
    
    @discardableResult
    open func result(completionHandler: @escaping (Result<Data, Error>) -> Void) -> DataManagerType {
        
        // Copy properties
        
        let properties = requestProperties
        
        // Reset properties
        
        requestProperties = RequestProperties()
        
        // Check if URLRequest is available
        
        guard let request = properties.request else {
            completionHandler(.failure(NetworkingError.corruptedURLRequest))
            return self
        }
        
        // Check if only offline should be used
        
        if properties.shouldUseCacheOnly {
            completionHandler(dataStorage.fetchData(for: request))
            return self
        }
        
        // Start API request
        
        networking
            .request(request)
            .validate(statusCode: properties.acceptableStatusCode)
            .validate(statusCodes: properties.acceptableStatusCodes)
            .result { [weak self] result in
                guard let weakSelf = self else {
                    completionHandler(.failure(NetworkingError.dataManagerUnavailable))
                    return
                }
                
                switch result {
                case .failure(let error):
                    if properties.shouldUseCacheOnError {
                        completionHandler(weakSelf.dataStorage.fetchData(for: request))
                        return
                    }
                    
                    completionHandler(.failure(error))
                    
                case .success(let value):
                    if properties.shouldUpdateCache {
                        weakSelf.dataStorage.updateData(for: request, data: value)
                    }
                    
                    completionHandler(result)
                }
        }
        
        return self
    }
    
}
