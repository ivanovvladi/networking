//
//  DataCoder.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2020 Vladislav Ivanov. All rights reserved.
//

import Foundation

public protocol DataCoderType: class {
    
    static var shared: DataCoderType { get }
    
    func decodeJSON<T: Codable>(from data: Result<Data, Error>) -> Result<T, Error>
    func decodeDictionary<T>(from data: Result<Data, Error>) -> Result<[String: T], Error>
    func encodeJSON<T: Codable>(from json: T) -> Result<Data, Error>
    
}

// TODO: Add data coder tests and documentation
open class DataCoder: DataCoderType {
    
    private let decoder: JSONDecoder
    private let encoder: JSONEncoder
    
    public static let shared: DataCoderType = DataCoder()
    
    public init(decoder: JSONDecoder = JSONDecoder(),
                encoder: JSONEncoder = JSONEncoder()) {
        
        self.encoder = encoder
        self.decoder = decoder
    }
    
    open func decodeJSON<T>(from data: Result<Data, Error>) -> Result<T, Error> where T: Codable {
        switch data {
        case .failure(let error):
            return .failure(error)
            
        case .success(let value):
            do {
                let data = try decoder.decode(T.self, from: value)
                return .success(data)
            } catch let error {
                return .failure(error)
            }
        }
    }
    
    open func decodeDictionary<T>(from data: Result<Data, Error>) -> Result<[String: T], Error> {
        switch data {
        case .failure(let error):
            return .failure(error)
            
        case .success(let value):
            do {
                let json = try JSONSerialization.jsonObject(with: value, options: [])
                
                guard let dictionary = json as? [String: T] else {
                    return .failure(NetworkingError.decodingFailed)
                }
                
                return .success(dictionary)
            } catch let error {
                return .failure(error)
            }
        }
    }
    
    open func encodeJSON<T: Codable>(from json: T) -> Result<Data, Error> {
        do {
            let data = try encoder.encode(json)
            return.success(data)
        } catch let error {
            return .failure(error)
        }
    }
    
}
