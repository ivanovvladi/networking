//
//  RequestBuilder.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import Foundation

// MARK: - APIEnviromentType

/// Defines API parameters
public protocol APIEnviromentType: class {
    
    /// Base API URL
    var apiURL: URL { get }
    
    /// Version of API
    var apiVersion: String? { get }
    
}

// MARK: - URLQueryType

/// Defines URL query parameters
public protocol URLQueryType {
    
    /// URL request query parameter, name-value pairs
    var query: [String: String] { get }
    
}

// MARK: - URLRequestHeaderType

/// Defines URL request headers
public protocol URLRequestHeaderType {
    
    /// URL request header, name-value pairs
    var header: [String: String] { get }
    
}

// MARK: - URLResponseType

/// Specifies URL response MIME type
public enum URLResponseType: String {
    
    /// JSON
    case json = "application/json"
    
    /// Any
    case none
}

// MARK: - RequestBuilder

/** Creates URL request
 
 # Example of usage: #
 
 ```
 RequestBuilder(APIEnviromentType)
 .add(pathComponent: PathComponent, PathComponent)
 .add(urlQuery: URLQueryType, URLQueryType)
 .add(header: URLRequestHeaderType, URLRequestHeaderType)
 .build()
 ```
 */
public protocol RequestBuilderType {
    
    /**
     Adds query parameters to URL
     
     - Parameters:
     - urlQuery: URL query parameters to append
     
     - Returns: Instance of Self
     */
    @discardableResult
    // sourcery: Variadic = pathComponent
    func add(pathComponent: PathComponent?...) -> RequestBuilderType
    
    /**
     Adds query parameters to URL
     
     - Parameters:
     - urlQuery: URL query parameters to append
     
     - Returns: Instance of Self
     */
    @discardableResult
    // sourcery: Variadic = urlQuery
    func add(urlQuery: URLQueryType?...) -> RequestBuilderType
    
    /**
     Adds query parameters to URL
     
     - Parameters:
     - urlQueries: URL query parameters to append
     
     - Returns: Instance of Self
     */
    @discardableResult
    func add(urlQueries: [URLQueryType]) -> RequestBuilderType
    
    /**
     Adds headers to URL request
     
     - Parameters:
     - header: URL request header to append
     
     - Returns: Instance of Self
     */
    @discardableResult
    // sourcery: Variadic = header
    func add(header: URLRequestHeaderType?...) -> RequestBuilderType
    
    /**
     Creates URL request
     - Parameters:
     - responseType: RequestBuilder.ResponseType
     
     - Returns: URLRequest
     */
    func build(responseType: URLResponseType) -> URLRequest
    
}

extension RequestBuilderType {
    
    /**
     Creates URL request
     - Parameters:
     - responseType: RequestBuilder.ResponseType, default is `json`
     
     - Returns: URLRequest
     */
    public func build(responseType: URLResponseType = .json) -> URLRequest {
        return build(responseType: responseType)
    }
    
}

open class RequestBuilder: RequestBuilderType {
    
    // MARK: - Properties
    
    /// Type to define API parameters
    private let apiEnviroment: APIEnviromentType
    
    /// URL query parameters for appending
    private var queryParams = [URLQueryType]()
    
    /// Path components for appending to URL
    private var pathComponents = [PathComponent]()
    
    /// URL request header for appending
    private var headers = [URLRequestHeaderType]()
    
    // MARK: - Init
    
    /**
     Create instance of the class RequestBuilder
     
     - Parameters:
     - apiEnviroment: APIEnviromentType
     */
    public init(_ apiEnviroment: APIEnviromentType) {
        self.apiEnviroment = apiEnviroment
    }
    
    // MARK: - Interface
    
    @discardableResult
    open func add(pathComponent: PathComponent?...) -> RequestBuilderType {
        pathComponents += pathComponent.compactMap { $0 }
        
        return self
    }
    
    @discardableResult
    open func add(urlQuery: URLQueryType?...) -> RequestBuilderType {
        queryParams += urlQuery.compactMap { $0 }
        
        return self
    }
    
    @discardableResult
    open func add(urlQueries: [URLQueryType]) -> RequestBuilderType {
        queryParams += urlQueries
        
        return self
    }
    
    @discardableResult
    open func add(header: URLRequestHeaderType?...) -> RequestBuilderType {
        headers += header.compactMap { $0 }
        
        return self
    }
    
    @discardableResult
    open func build(responseType: URLResponseType = .json) -> URLRequest {
        
        // Combine all path components
        
        let path = getPathComponents().reduce("") { result, pathComponent in
            return result.appending("/").appending(pathComponent)
        }
        
        var url = apiEnviroment.apiURL
        
        // Create base URL
        
        if let apiVersion = apiEnviroment.apiVersion {
            url = url.appendingPathComponent("/\(apiVersion)")
        }
        
        // Append path components if avaliable
        
        if !path.isEmpty {
            url = url.appendingPathComponent(path)
        }
        
        // Append header for expected respone MIME type
        
        switch responseType {
        case .json:
            headers.append(JSONMIMETypeHeader())
            
        case .none:
            break
        }
        
        // Create request
        
        return URLRequest(url: URL(url: url, urlQuery: getQueryParams())!,
                          header: getHeaders())
    }
    
}

// MARK: - Helpers

extension RequestBuilder {
    
    /// Header `["Accept": "application/json"]` expected JSON respone MIME type
    fileprivate struct JSONMIMETypeHeader: URLRequestHeaderType {
        let header = ["Accept": "application/json"]
    }
    
    /**
     Gets current array of path components and then resets
     
     - Returns: Array of path components
     */
    fileprivate func getPathComponents() -> [PathComponent] {
        let components = pathComponents
        
        pathComponents = []
        
        return components
    }
    
    /**
     Gets current array of URL query parameters and then resets
     
     - Returns: Array of URL query parameters
     */
    fileprivate func getQueryParams() -> [URLQueryType] {
        let params = queryParams
        
        queryParams = []
        
        return params
    }
    
    /**
     Gets current array of URL request headers and then resets
     
     - Returns: Array of URL request headers
     */
    fileprivate func getHeaders() -> [URLRequestHeaderType] {
        let headers = self.headers
        
        self.headers = []
        
        return headers
    }
    
}

extension URL {
    
    /**
     Creates an URL
     
     - Parameters:
     - url: URL endpoint for API
     - urlQuery: array of URL query parameters
     */
    fileprivate init?(url: URL, urlQuery: [URLQueryType]) {
        let converted = urlQuery.reduce([(key: String, value: String)](), { result, next in
            return result + next.query.map { param -> (key: String, value: String) in
                return (key: param.key, value: param.value)
            }
        })
        
        var components = URLComponents(string: url.absoluteString)
        
        if urlQuery.count > 0 {
            components?.queryItems = converted.map { key, value in
                URLQueryItem(name: key, value: value)
            }
        }
        
        guard let absoluteString = components?.url?.absoluteString else {
            return nil
        }
        
        // Make URL string encoded. For some reason, addingPercentEncoding doesn't work correctly
        
        let encoded = absoluteString.replacingOccurrences(of: ",", with: "%2C")
        
        self.init(string: encoded)
    }
    
}

extension URLRequest {
    
    /**
     Creates an URL request
     
     - Parameters:
     - url: URL endpoint for API
     - header: array of URL request headers
     */
    fileprivate init(url: URL, header: [URLRequestHeaderType]) {
        var request = URLRequest(url: url)
        
        request.allHTTPHeaderFields = [String: String]()
            .merging(request.allHTTPHeaderFields ?? [String: String](), uniquingKeysWith: { left, _ in
                left
            })
            .merging(header.reduce([String: String](), { result, next in
                return result.merging(next.header, uniquingKeysWith: { left, _ in
                    left
                })
            }), uniquingKeysWith: { _, right in
                right
            })
        
        self = request
    }
    
}
