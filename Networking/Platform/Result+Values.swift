//
//  Result+Values.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2020 Vladislav Ivanov. All rights reserved.
//

import Foundation

extension Result {
    
    public var value: Success? {
        switch self {
        case .success(let value):
            return value
            
        default:
            return nil
        }
    }
    
    public var error: Error? {
        switch self {
        case .failure(let error):
            return error
            
        default:
            return nil
        }
    }
    
}
