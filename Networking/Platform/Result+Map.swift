//
//  Result+Map.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2020 Vladislav Ivanov. All rights reserved.
//

import Foundation

extension Result where Success == Data, Failure == Error {
    
    // MARK: - Map
    
    public func map<T: Codable>(with dataManager: DataManagerType?) -> Result<T, Error> {
        guard let dataManager = dataManager else {
            return .failure(NetworkingError.dataManagerUnavailable)
        }
        
        return dataManager.dataCoder.decodeJSON(from: self)
    }
    
    public func map<T: Codable>(with dataCoder: DataCoderType = DataCoder.shared) -> Result<T, Error> {
        return dataCoder.decodeJSON(from: self)
    }
    
    // MARK: - Map to dictionary
    
    public func mapToDictionary<T>(with dataManager: DataManagerType?) -> Result<[String: T], Error> {
        guard let dataManager = dataManager else {
            return .failure(NetworkingError.dataManagerUnavailable)
        }
        
        return dataManager.dataCoder.decodeDictionary(from: self)
    }
    
    public func mapToDictionary<T: Codable>(
        with dataCoder: DataCoderType = DataCoder.shared) -> Result<[String: T], Error> {
        
        return dataCoder.decodeDictionary(from: self)
    }
    
}
