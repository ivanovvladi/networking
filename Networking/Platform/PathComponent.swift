//
//  PathComponent.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2020 Vladislav Ivanov. All rights reserved.
//

import Foundation

public typealias PathComponent = String
