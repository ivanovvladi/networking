//
//  DataStorage.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import Foundation

public protocol DataStorageType: class {
    
    /// Returns singleton instance of Self
    static var shared: DataStorageType { get }
    
    /**
    Returns result of fetching offline data for URL request with data or error
    - Parameters:
    - urlRequest: URLRequest to execute
    
    - Returns: Result<Data, Error>
    */
    @discardableResult
    func fetchData(for urlRequest: URLRequest) -> Result<Data, Error>
    
    /**
     Returns instance of Self and updates cache for URL request with data
     
     - Parameters:
     - urlRequest: URL request for which cache should be updated
     - data: Data which should be saved
     
     - Returns: instance of Self
     */
    @discardableResult
    func updateData(for urlRequest: URLRequest, data: Data) -> DataStorageType
    
    /**
     Returns instance of Self and cleans cache for URL request
     
     - Parameters:
     - urlRequest: URL request for which cache should be cleaned
     
     - Returns: instance of Self
     */
    @discardableResult
    func cleanData(for urlRequest: URLRequest) -> DataStorageType
    
    /**
     Returns instance of Self and cleans whole stored cache
     
     - Returns: instance of Self
     */
    @discardableResult
    func cleanAll() -> DataStorageType
    
}

// TODO: Add data storage tests
open class DataStorage: DataStorageType {
    
    // MARK: - Properties
    
    /// File manager which provides the possibility to copy/move/enumerate/etc resources
    private let fileManager: FileManager
    
    /// The URL of the folder where a data will be stored
    private let directoryURL: URL?
    
    // MARK: - Public properties
    public static let shared: DataStorageType = DataStorage()
    
    /**
     Create instance of Self
     
     - Parameters:
     - folder: Folder which will be created in document directory for storing cache,
     default is `NetworkingDataStorage`
     - fileManager: FileManager, default is `FileManager.default`
     */
    public init(folder: PathComponent = "NetworkingDataStorage",
                fileManager: FileManager = .default) {
        
        self.fileManager = fileManager
        
        let url = try? fileManager.url(for: .documentDirectory,
                                       in: .userDomainMask,
                                       appropriateFor: nil,
                                       create: false)
            .appendingPathComponent(folder)
        
        self.directoryURL = url
        
        if let url = url {
            try? fileManager.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        }
    }
    
    open func fetchData(for urlRequest: URLRequest) -> Result<Data, Error> {
        guard let fileName = generateFileNameFrom(urlRequest: urlRequest) else {
            return .failure(NetworkingError.corruptedURLRequest)
        }
        
        guard let url = directoryURL?.appendingPathComponent("\(fileName).data") else {
            return .failure(NetworkingError.cacheDirectoryUnavailable)
        }
        
        guard let data = try? Data(contentsOf: url) else {
            return .failure(NetworkingError.corruptedCache)
        }
        
        return .success(data)
    }
    
    open func updateData(for urlRequest: URLRequest, data: Data) -> DataStorageType {
        guard let fileName = generateFileNameFrom(urlRequest: urlRequest) else {
            return self
        }
        
        guard let url = directoryURL?.appendingPathComponent("\(fileName).data") else {
            return self
        }
        
        try? data.write(to: url)
        
        return self
    }
    
    open func cleanData(for urlRequest: URLRequest) -> DataStorageType {
        guard let fileName = generateFileNameFrom(urlRequest: urlRequest) else {
            return self
        }
        
        guard let url = directoryURL?.appendingPathComponent("\(fileName).data") else {
            return self
        }
        
        try? fileManager.removeItem(at: url)
        
        return self
    }
    
    open func cleanAll() -> DataStorageType {
        guard let url = directoryURL else {
            return self
        }
        
        try? fileManager.removeItem(at: url)
        
        return self
    }
    
}

// MARK: - Helpers

extension DataStorage {
    
    /**
    Generates and returns file name for URL request
    
    - Parameters:
    - urlRequest: URL request for which file name will be generated
    
    - Returns: generated file name for URL request
    */
    fileprivate func generateFileNameFrom(urlRequest: URLRequest) -> String? {
        return urlRequest.url?.absoluteString
            .replacingOccurrences(of: urlRequest.url?.host ?? " ", with: "")
            .replacingOccurrences(of: urlRequest.url?.scheme ?? " ", with: "")
            .replacingOccurrences(of: ":", with: "")
            .replacingOccurrences(of: "/", with: "-")
            .replacingOccurrences(of: ".", with: "-")
    }
    
}
