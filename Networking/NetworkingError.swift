//
//  NetworkingError.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2019 Vladislav Ivanov. All rights reserved.
//

import Foundation

/// `NetworkingError` is the error type returned by Networking
public enum NetworkingError: Error, Equatable {
    
    /// URL request is nil or incorrect
    case corruptedURLRequest
    
    /// Cache directory in document directory is unavailable
    case cacheDirectoryUnavailable
    
    /// Cache is nil or incorrect
    case corruptedCache
    
    /// Data manager has been disposed earlier than the response came
    case dataManagerUnavailable
    
    /// Data is nil or incorrect
    case corruptedData
    
    /// Data is empty
    case emptyData
    
    /// URL response is nil or incorrect
    case urlResponseCorrupted
    
    /// URL response has invalid status code
    case statusCodeInvalid
    
    /// Decoding failed
    case decodingFailed
    
}
