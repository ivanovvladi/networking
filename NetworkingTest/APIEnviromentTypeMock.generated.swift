// Generated using Sourcery 1.0.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

import Networking

open class APIEnviromentTypeMock: APIEnviromentType {

    public init() {

    }

    public var apiURL: URL {
        get { 
            return underlyingApiURL 
        }
        set { 
            underlyingApiURL = newValue 
        }
    }
    public var underlyingApiURL: URL!

    public var apiVersion: String?

}
