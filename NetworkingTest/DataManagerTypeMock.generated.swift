// Generated using Sourcery 1.0.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

import Networking

open class DataManagerTypeMock: DataManagerType {

    public init() {

    }

    public var dataCoder: DataCoderType {
        get { 
            return underlyingDataCoder 
        }
        set { 
            underlyingDataCoder = newValue 
        }
    }
    public var underlyingDataCoder: DataCoderType = DataCoder()

    public var requestBuilder: RequestBuilderType {
        get { 
            return underlyingRequestBuilder 
        }
        set { 
            underlyingRequestBuilder = newValue 
        }
    }
    public var underlyingRequestBuilder: RequestBuilderType = RequestBuilderTypeMock()

    // MARK: - fetchCache

    public var fetchCacheForCallsCount = 0
    public var fetchCacheForCalled: Bool {
        return fetchCacheForCallsCount > 0
    }
    public var fetchCacheForReceivedUrlRequest: URLRequest?
    public var fetchCacheForReceivedInvocations: [URLRequest] = []
    public var fetchCacheForReturnValue: Result<Data, Error>!
    public var fetchCacheForClosure: ((URLRequest) -> Result<Data, Error>)?

    open func fetchCache(for urlRequest: URLRequest) -> Result<Data, Error> {
        fetchCacheForCallsCount += 1
        fetchCacheForReceivedUrlRequest = urlRequest
        fetchCacheForReceivedInvocations.append(urlRequest)
        return fetchCacheForClosure.map({ $0(urlRequest) }) ?? fetchCacheForReturnValue
    }

    // MARK: - updateCache

    public var updateCacheForDataCallsCount = 0
    public var updateCacheForDataCalled: Bool {
        return updateCacheForDataCallsCount > 0
    }
    public var updateCacheForDataReceivedArguments: (urlRequest: URLRequest, data: Data)?
    public var updateCacheForDataReceivedInvocations: [(urlRequest: URLRequest, data: Data)] = []
    public lazy var updateCacheForDataReturnValue: DataManagerType = self
    public var updateCacheForDataClosure: ((URLRequest, Data) -> DataManagerType)?

    open func updateCache(for urlRequest: URLRequest, data: Data) -> DataManagerType {
        updateCacheForDataCallsCount += 1
        updateCacheForDataReceivedArguments = (urlRequest: urlRequest, data: data)
        updateCacheForDataReceivedInvocations.append((urlRequest: urlRequest, data: data))
        return updateCacheForDataClosure.map({ $0(urlRequest, data) }) ?? updateCacheForDataReturnValue
    }

    // MARK: - cleanCache

    public var cleanCacheForCallsCount = 0
    public var cleanCacheForCalled: Bool {
        return cleanCacheForCallsCount > 0
    }
    public var cleanCacheForReceivedUrlRequest: URLRequest?
    public var cleanCacheForReceivedInvocations: [URLRequest] = []
    public lazy var cleanCacheForReturnValue: DataManagerType = self
    public var cleanCacheForClosure: ((URLRequest) -> DataManagerType)?

    open func cleanCache(for urlRequest: URLRequest) -> DataManagerType {
        cleanCacheForCallsCount += 1
        cleanCacheForReceivedUrlRequest = urlRequest
        cleanCacheForReceivedInvocations.append(urlRequest)
        return cleanCacheForClosure.map({ $0(urlRequest) }) ?? cleanCacheForReturnValue
    }

    // MARK: - cleanAllCache

    public var cleanAllCacheCallsCount = 0
    public var cleanAllCacheCalled: Bool {
        return cleanAllCacheCallsCount > 0
    }
    public lazy var cleanAllCacheReturnValue: DataManagerType = self
    public var cleanAllCacheClosure: (() -> DataManagerType)?

    open func cleanAllCache() -> DataManagerType {
        cleanAllCacheCallsCount += 1
        return cleanAllCacheClosure.map({ $0() }) ?? cleanAllCacheReturnValue
    }

    // MARK: - fetch

    public var fetchCallsCount = 0
    public var fetchCalled: Bool {
        return fetchCallsCount > 0
    }
    public var fetchReceivedUrlRequest: URLRequest?
    public var fetchReceivedInvocations: [URLRequest] = []
    public lazy var fetchReturnValue: DataManagerType = self
    public var fetchClosure: ((URLRequest) -> DataManagerType)?

    open func fetch(_ urlRequest: URLRequest) -> DataManagerType {
        fetchCallsCount += 1
        fetchReceivedUrlRequest = urlRequest
        fetchReceivedInvocations.append(urlRequest)
        return fetchClosure.map({ $0(urlRequest) }) ?? fetchReturnValue
    }

    // MARK: - validate

    public var validateStatusCodeCallsCount = 0
    public var validateStatusCodeCalled: Bool {
        return validateStatusCodeCallsCount > 0
    }
    public var validateStatusCodeReceivedStatusCode: Int?
    public var validateStatusCodeReceivedInvocations: [Int] = []
    public lazy var validateStatusCodeReturnValue: DataManagerType = self
    public var validateStatusCodeClosure: ((Int) -> DataManagerType)?

    open func validate(statusCode: Int) -> DataManagerType {
        validateStatusCodeCallsCount += 1
        validateStatusCodeReceivedStatusCode = statusCode
        validateStatusCodeReceivedInvocations.append(statusCode)
        return validateStatusCodeClosure.map({ $0(statusCode) }) ?? validateStatusCodeReturnValue
    }

    // MARK: - validate

    public var validateStatusCodesCallsCount = 0
    public var validateStatusCodesCalled: Bool {
        return validateStatusCodesCallsCount > 0
    }
    public var validateStatusCodesReceivedStatusCodes: Range<Int>?
    public var validateStatusCodesReceivedInvocations: [Range<Int>] = []
    public lazy var validateStatusCodesReturnValue: DataManagerType = self
    public var validateStatusCodesClosure: ((Range<Int>) -> DataManagerType)?

    open func validate(statusCodes: Range<Int>) -> DataManagerType {
        validateStatusCodesCallsCount += 1
        validateStatusCodesReceivedStatusCodes = statusCodes
        validateStatusCodesReceivedInvocations.append(statusCodes)
        return validateStatusCodesClosure.map({ $0(statusCodes) }) ?? validateStatusCodesReturnValue
    }

    // MARK: - updateCache

    public var updateCacheCallsCount = 0
    public var updateCacheCalled: Bool {
        return updateCacheCallsCount > 0
    }
    public lazy var updateCacheReturnValue: DataManagerType = self
    public var updateCacheClosure: (() -> DataManagerType)?

    open func updateCache() -> DataManagerType {
        updateCacheCallsCount += 1
        return updateCacheClosure.map({ $0() }) ?? updateCacheReturnValue
    }

    // MARK: - useCacheOnError

    public var useCacheOnErrorCallsCount = 0
    public var useCacheOnErrorCalled: Bool {
        return useCacheOnErrorCallsCount > 0
    }
    public lazy var useCacheOnErrorReturnValue: DataManagerType = self
    public var useCacheOnErrorClosure: (() -> DataManagerType)?

    open func useCacheOnError() -> DataManagerType {
        useCacheOnErrorCallsCount += 1
        return useCacheOnErrorClosure.map({ $0() }) ?? useCacheOnErrorReturnValue
    }

    // MARK: - useCacheOnly

    public var useCacheOnlyCallsCount = 0
    public var useCacheOnlyCalled: Bool {
        return useCacheOnlyCallsCount > 0
    }
    public lazy var useCacheOnlyReturnValue: DataManagerType = self
    public var useCacheOnlyClosure: (() -> DataManagerType)?

    open func useCacheOnly() -> DataManagerType {
        useCacheOnlyCallsCount += 1
        return useCacheOnlyClosure.map({ $0() }) ?? useCacheOnlyReturnValue
    }

    // MARK: - result

    public var resultCompletionHandlerCallsCount = 0
    public var resultCompletionHandlerCalled: Bool {
        return resultCompletionHandlerCallsCount > 0
    }
    public var resultCompletionHandlerReceivedCompletionHandler: ((Result<Data, Error>) -> Void)?
    public var resultCompletionHandlerReceivedInvocations: [((Result<Data, Error>) -> Void)] = []
    public lazy var resultCompletionHandlerReturnValue: DataManagerType = self
    public var resultCompletionHandlerClosure: ((@escaping (Result<Data, Error>) -> Void) -> DataManagerType)?

    open func result(completionHandler: @escaping (Result<Data, Error>) -> Void) -> DataManagerType {
        resultCompletionHandlerCallsCount += 1
        resultCompletionHandlerReceivedCompletionHandler = completionHandler
        resultCompletionHandlerReceivedInvocations.append(completionHandler)
        return resultCompletionHandlerClosure.map({ $0(completionHandler) }) ?? resultCompletionHandlerReturnValue
    }

}
