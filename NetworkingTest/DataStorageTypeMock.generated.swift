// Generated using Sourcery 1.0.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

import Networking

open class DataStorageTypeMock: DataStorageType {

    public init() {

    }

    public static var shared: DataStorageType {
        get { 
            return underlyingShared 
        }
        set { 
            underlyingShared = newValue 
        }
    }
    public static var underlyingShared: DataStorageType = DataStorageTypeMock()

    // MARK: - fetchData

    public var fetchDataForCallsCount = 0
    public var fetchDataForCalled: Bool {
        return fetchDataForCallsCount > 0
    }
    public var fetchDataForReceivedUrlRequest: URLRequest?
    public var fetchDataForReceivedInvocations: [URLRequest] = []
    public var fetchDataForReturnValue: Result<Data, Error>!
    public var fetchDataForClosure: ((URLRequest) -> Result<Data, Error>)?

    open func fetchData(for urlRequest: URLRequest) -> Result<Data, Error> {
        fetchDataForCallsCount += 1
        fetchDataForReceivedUrlRequest = urlRequest
        fetchDataForReceivedInvocations.append(urlRequest)
        return fetchDataForClosure.map({ $0(urlRequest) }) ?? fetchDataForReturnValue
    }

    // MARK: - updateData

    public var updateDataForDataCallsCount = 0
    public var updateDataForDataCalled: Bool {
        return updateDataForDataCallsCount > 0
    }
    public var updateDataForDataReceivedArguments: (urlRequest: URLRequest, data: Data)?
    public var updateDataForDataReceivedInvocations: [(urlRequest: URLRequest, data: Data)] = []
    public lazy var updateDataForDataReturnValue: DataStorageType = self
    public var updateDataForDataClosure: ((URLRequest, Data) -> DataStorageType)?

    open func updateData(for urlRequest: URLRequest, data: Data) -> DataStorageType {
        updateDataForDataCallsCount += 1
        updateDataForDataReceivedArguments = (urlRequest: urlRequest, data: data)
        updateDataForDataReceivedInvocations.append((urlRequest: urlRequest, data: data))
        return updateDataForDataClosure.map({ $0(urlRequest, data) }) ?? updateDataForDataReturnValue
    }

    // MARK: - cleanData

    public var cleanDataForCallsCount = 0
    public var cleanDataForCalled: Bool {
        return cleanDataForCallsCount > 0
    }
    public var cleanDataForReceivedUrlRequest: URLRequest?
    public var cleanDataForReceivedInvocations: [URLRequest] = []
    public lazy var cleanDataForReturnValue: DataStorageType = self
    public var cleanDataForClosure: ((URLRequest) -> DataStorageType)?

    open func cleanData(for urlRequest: URLRequest) -> DataStorageType {
        cleanDataForCallsCount += 1
        cleanDataForReceivedUrlRequest = urlRequest
        cleanDataForReceivedInvocations.append(urlRequest)
        return cleanDataForClosure.map({ $0(urlRequest) }) ?? cleanDataForReturnValue
    }

    // MARK: - cleanAll

    public var cleanAllCallsCount = 0
    public var cleanAllCalled: Bool {
        return cleanAllCallsCount > 0
    }
    public lazy var cleanAllReturnValue: DataStorageType = self
    public var cleanAllClosure: (() -> DataStorageType)?

    open func cleanAll() -> DataStorageType {
        cleanAllCallsCount += 1
        return cleanAllClosure.map({ $0() }) ?? cleanAllReturnValue
    }

}
