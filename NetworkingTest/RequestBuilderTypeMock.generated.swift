// Generated using Sourcery 1.0.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

import Networking

open class RequestBuilderTypeMock: RequestBuilderType {

    public init() {

    }

    // MARK: - add

    public var addPathComponentCallsCount = 0
    public var addPathComponentCalled: Bool {
        return addPathComponentCallsCount > 0
    }
    public var addPathComponentReceivedPathComponent: [PathComponent?]?
    public var addPathComponentReceivedInvocations: [PathComponent?] = []
    public lazy var addPathComponentReturnValue: RequestBuilderType = self
    public var addPathComponentClosure: (([PathComponent?]) -> RequestBuilderType)?

    open func add(pathComponent: PathComponent?...) -> RequestBuilderType {
        addPathComponentCallsCount += 1
        addPathComponentReceivedPathComponent = pathComponent
        addPathComponentReceivedInvocations.append(contentsOf: pathComponent)
        return addPathComponentClosure.map({ $0(pathComponent) }) ?? addPathComponentReturnValue
    }

    // MARK: - add

    public var addUrlQueryCallsCount = 0
    public var addUrlQueryCalled: Bool {
        return addUrlQueryCallsCount > 0
    }
    public var addUrlQueryReceivedUrlQuery: [URLQueryType?]?
    public var addUrlQueryReceivedInvocations: [URLQueryType?] = []
    public lazy var addUrlQueryReturnValue: RequestBuilderType = self
    public var addUrlQueryClosure: (([URLQueryType?]) -> RequestBuilderType)?

    open func add(urlQuery: URLQueryType?...) -> RequestBuilderType {
        addUrlQueryCallsCount += 1
        addUrlQueryReceivedUrlQuery = urlQuery
        addUrlQueryReceivedInvocations.append(contentsOf: urlQuery)
        return addUrlQueryClosure.map({ $0(urlQuery) }) ?? addUrlQueryReturnValue
    }

    // MARK: - add

    public var addUrlQueriesCallsCount = 0
    public var addUrlQueriesCalled: Bool {
        return addUrlQueriesCallsCount > 0
    }
    public var addUrlQueriesReceivedUrlQueries: [URLQueryType]?
    public var addUrlQueriesReceivedInvocations: [[URLQueryType]] = []
    public lazy var addUrlQueriesReturnValue: RequestBuilderType = self
    public var addUrlQueriesClosure: (([URLQueryType]) -> RequestBuilderType)?

    open func add(urlQueries: [URLQueryType]) -> RequestBuilderType {
        addUrlQueriesCallsCount += 1
        addUrlQueriesReceivedUrlQueries = urlQueries
        addUrlQueriesReceivedInvocations.append(urlQueries)
        return addUrlQueriesClosure.map({ $0(urlQueries) }) ?? addUrlQueriesReturnValue
    }

    // MARK: - add

    public var addHeaderCallsCount = 0
    public var addHeaderCalled: Bool {
        return addHeaderCallsCount > 0
    }
    public var addHeaderReceivedHeader: [URLRequestHeaderType?]?
    public var addHeaderReceivedInvocations: [URLRequestHeaderType?] = []
    public lazy var addHeaderReturnValue: RequestBuilderType = self
    public var addHeaderClosure: (([URLRequestHeaderType?]) -> RequestBuilderType)?

    open func add(header: URLRequestHeaderType?...) -> RequestBuilderType {
        addHeaderCallsCount += 1
        addHeaderReceivedHeader = header
        addHeaderReceivedInvocations.append(contentsOf: header)
        return addHeaderClosure.map({ $0(header) }) ?? addHeaderReturnValue
    }

    // MARK: - build

    public var buildResponseTypeCallsCount = 0
    public var buildResponseTypeCalled: Bool {
        return buildResponseTypeCallsCount > 0
    }
    public var buildResponseTypeReceivedResponseType: URLResponseType?
    public var buildResponseTypeReceivedInvocations: [URLResponseType] = []
    public var buildResponseTypeReturnValue: URLRequest!
    public var buildResponseTypeClosure: ((URLResponseType) -> URLRequest)?

    open func build(responseType: URLResponseType) -> URLRequest {
        buildResponseTypeCallsCount += 1
        buildResponseTypeReceivedResponseType = responseType
        buildResponseTypeReceivedInvocations.append(responseType)
        return buildResponseTypeClosure.map({ $0(responseType) }) ?? buildResponseTypeReturnValue
    }

}
