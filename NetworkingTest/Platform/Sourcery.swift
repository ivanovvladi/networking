//
//  Sourcery.swift
//  Networking
//
//  Created by Vladislav Ivanov.
//  Copyright © 2020 Vladislav Ivanov. All rights reserved.
//

import Networking

// sourcery: AutoMockable
extension APIEnviromentType {}

// sourcery: AutoMockable
extension NetworkingType {}

// sourcery: AutoMockable
extension DataManagerType {}

// sourcery: AutoMockable
extension DataStorageType {}

// sourcery: AutoMockable
extension RequestBuilderType {}

// sourcery: CallableInit=DataCoder
extension DataCoderType {}
