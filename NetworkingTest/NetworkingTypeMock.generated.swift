// Generated using Sourcery 1.0.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

import Networking

open class NetworkingTypeMock: NetworkingType {

    public init() {

    }

    public static var shared: NetworkingType {
        get { 
            return underlyingShared 
        }
        set { 
            underlyingShared = newValue 
        }
    }
    public static var underlyingShared: NetworkingType = NetworkingTypeMock()

    // MARK: - request

    public var requestCallsCount = 0
    public var requestCalled: Bool {
        return requestCallsCount > 0
    }
    public var requestReceivedUrlRequest: URLRequest?
    public var requestReceivedInvocations: [URLRequest] = []
    public lazy var requestReturnValue: NetworkingType = self
    public var requestClosure: ((URLRequest) -> NetworkingType)?

    open func request(_ urlRequest: URLRequest) -> NetworkingType {
        requestCallsCount += 1
        requestReceivedUrlRequest = urlRequest
        requestReceivedInvocations.append(urlRequest)
        return requestClosure.map({ $0(urlRequest) }) ?? requestReturnValue
    }

    // MARK: - validate

    public var validateStatusCodesCallsCount = 0
    public var validateStatusCodesCalled: Bool {
        return validateStatusCodesCallsCount > 0
    }
    public var validateStatusCodesReceivedStatusCodes: Range<Int>?
    public var validateStatusCodesReceivedInvocations: [Range<Int>?] = []
    public lazy var validateStatusCodesReturnValue: NetworkingType = self
    public var validateStatusCodesClosure: ((Range<Int>?) -> NetworkingType)?

    open func validate(statusCodes: Range<Int>?) -> NetworkingType {
        validateStatusCodesCallsCount += 1
        validateStatusCodesReceivedStatusCodes = statusCodes
        validateStatusCodesReceivedInvocations.append(statusCodes)
        return validateStatusCodesClosure.map({ $0(statusCodes) }) ?? validateStatusCodesReturnValue
    }

    // MARK: - validate

    public var validateStatusCodeCallsCount = 0
    public var validateStatusCodeCalled: Bool {
        return validateStatusCodeCallsCount > 0
    }
    public var validateStatusCodeReceivedStatusCode: Int?
    public var validateStatusCodeReceivedInvocations: [Int?] = []
    public lazy var validateStatusCodeReturnValue: NetworkingType = self
    public var validateStatusCodeClosure: ((Int?) -> NetworkingType)?

    open func validate(statusCode: Int?) -> NetworkingType {
        validateStatusCodeCallsCount += 1
        validateStatusCodeReceivedStatusCode = statusCode
        validateStatusCodeReceivedInvocations.append(statusCode)
        return validateStatusCodeClosure.map({ $0(statusCode) }) ?? validateStatusCodeReturnValue
    }

    // MARK: - result

    public var resultCompletionHandlerCallsCount = 0
    public var resultCompletionHandlerCalled: Bool {
        return resultCompletionHandlerCallsCount > 0
    }
    public var resultCompletionHandlerReceivedCompletionHandler: ((Result<Data, Error>) -> Void)?
    public var resultCompletionHandlerReceivedInvocations: [((Result<Data, Error>) -> Void)] = []
    public var resultCompletionHandlerReturnValue: URLSessionDataTask?
    public var resultCompletionHandlerClosure: ((@escaping (Result<Data, Error>) -> Void) -> URLSessionDataTask?)?

    open func result(completionHandler: @escaping (Result<Data, Error>) -> Void) -> URLSessionDataTask? {
        resultCompletionHandlerCallsCount += 1
        resultCompletionHandlerReceivedCompletionHandler = completionHandler
        resultCompletionHandlerReceivedInvocations.append(completionHandler)
        return resultCompletionHandlerClosure.map({ $0(completionHandler) }) ?? resultCompletionHandlerReturnValue
    }

}
